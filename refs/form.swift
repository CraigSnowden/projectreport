form +++ Section()
  <<< ButtonRow("Favourite Stops") {
      $0.title = "Favourite Stops"
      $0.presentationMode = .SegueName(segueName: "Favourites",
              completionCallback: nil)
      }
  +++ Section("Location")
  <<< ButtonRow("Choose location") {
      $0.title = "Choose location"
      }.onCellSelection { _,_ in
          self.chooseLocation()
      }
  <<< SwitchRow() {
      $0.title = "Use my current location"
      $0.value = false
      }.onChange { state in
          // request location
      }
  <<< ButtonRow("Search") {
      $0.title = row.tag
      $0.presentationMode = .SegueName(segueName: "Search Results",
              completionCallback: nil)
  }
