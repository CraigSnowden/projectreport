\section{iPhone application}
\label{chapter:ios}
\subsection{Choice of language}
The chosen platform for this application is iOS. This was stipulated by the project proposal, however it also makes a good choice for an accessibility oriented mobile application due to the ubiquitous availability of the VoiceOver screenreader on Apple devices. For Android, the equivalent TalkBack feature is not necessarily available on all commercially available devices due to the device fragmentation introduced by an open platform.

The number of frameworks available to develop iOS applications are ever growing, though it is acknowledged that developing with the native languages (Swift and Objective C) against the native framework Cocoa allows the developer more direct access to the features of the SDK. Many cross platform options utilise a embedded web page, allowing development in HTML/JavaScript, however they do not utilise the operating system user interface elements and so are not as accessible. Cross platform/Native API options such as Xamarin are an interesting option however as we have chosen to target iOS exclusively the cross platform aspect is of little use.

Swift is very new as a language -- having been introduced in June 2014 -- but is also the language that new features are going to be developed first in over Objective C. Thus Swift is the recommended language for starting new projects, and indeed this one. Unlike Objective C, Swift handles memory management automatically, so the associated boilerplate is significantly reduced.


\subsection{User interface}

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{images/mocks.pdf}
\caption{Mobile application mockups}
    \label{fig:ios_mocks}
\end{figure}

For an accessible application, consideration of the user needs and accessibility software requirements is essential. Thus, utilising low overhead prototyping ensures that the implementation can take place in a more guided fashion. In Figure \ref{fig:ios_mocks}, the mobile application is laid out in a storyboard format giving an overview of the potential user interaction.

The iOS platform offers multiple approaches to creating a user interface: in code, through creating individual \ac{XIB} files or through building storyboards from linked \ac{XIB} files. Storyboards were used  (seen in Figure \ref{fig:xcode_story}) allowing the transitions between views to be implemented without code.

This is reminiscent of the mockup in Figure \ref{fig:ios_mocks}, and means that the UI can be further tested prior to implementing the logic. I decided against using individual \ac{XIB}s as this would make visualising the paths of the UI more difficult, especially when there are multiple routes to reach certain views.

Specifying the User Interface in code would be possible (and indeed some of the views are implemented in a \ac{DSL}, mentioned in Section \ref{sec:form-classes}), however this would require significantly more code for little to no gain.

\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{images/xcode-storyboard.png}
\caption{User interface design translated to a UIKit Storyboard}
    \label{fig:xcode_story}
\end{figure}

\subsubsection{Stops}
This screen should allow the user to search for a stop in either their local area (with GPS location) or by searching for a particular location. The stops near this location will then be returned, alongside the bus routes that visit the stop. When the user selects a stop they should be able to view a list of real time departures with route name, route destination and time of departure. Having selected a departure, the user should be able to view when that particular bus will reach each of the stops it calls at.

\subsubsection{Routes}
This screen should allow the user to search for a particular bus route, given its name and the location in which it operates (to distinguish it from other routes of the same name operating in different areas). A list of matching routes will be returned, and the user can choose a route to view the stops it visits. Selecting a stop will show the upcoming departures from that stop.

\subsubsection{Directions}
This screen should allow the user to acquire point to point navigation instructions from a given initial and destination location (inputted either through search or GPS).

\subsection{Implementation}
\subsubsection{Form classes}
\label{sec:form-classes}
The three search views (stops, routes and directions) make use of a table based form, which can be difficult to layout when it comes to placing individual UI elements. The Eureka\cite{Eureka} form \ac{DSL} was used, allowing forms to be constructed in a consistent manner.

The library is provided with an empty view that is fit into the existing storyboard layout, which can then be populated by inheriting from a provided class. The form can then be built by specifying the sections and fields in code, like demonstrated in Figure \ref{fig:eureka}.

Although not implemented in this project, custom form elements can be designed to accommodate for particular use cases: future work for this project could involve implementing a ``Location'' field that allows the user to look up a location with autocomplete. For this project we implement location selection using the `onCellSelect' callback function which presents a modal dialog provided by the location autocomplete library\cite{googleplaces}.

\begin{figure}[h]
\centering
\lstinputlisting[basicstyle=\small, escapeinside={\#*}{*)} ]{refs/form.swift}
\caption{Example of a Eureka form}
\label{fig:eureka}
\end{figure}

\subsubsection{Swift Extensions}
One of the useful features of Swift is the ability to define extensions of a class. Extensions provide a way to augment an existing class with further methods without the need to create a subclass, which can be in a separate file to the original implementation.

This is particularly useful in iOS, as many of the built in functions and 3rd party libraries make use of a delegate pattern described in Table \ref{table:delegate}.

\begin{table}[ht]
\centering
\caption{Important definitions in the delegate pattern}
\label{table:delegate}
\begin{tabular}{| p{0.32\textwidth} | p{0.32\textwidth} | p{0.32\textwidth} |}
\textbf{Protocol}                   & \textbf{Delegate}      & \textbf{Delegator}                                    \\
Provides a specification to a class & Conforms to a protocol & Requires a delegate class that conforms to a protocol \\
`Will do X'                          & `Can do X'               & `Will tell something to do X'
\end{tabular}
\end{table}

In the case of the Apple provided UITableView -- a class which provides a UI element -- a delegate is used to supply the data to the table. In this case UITableViewDelegate is the protocol, UITableView is the delegator and a custom view controller is the delegator. By using extensions the methods used to implement the delegate protocol can be kept separately, drastically improving the readability of classes.

\subsubsection{Networking}
The Alamofire library\cite{Alamofire} was used to provide a wrapper around the built in HTTP request methods, allowing network requests to take place asynchronously.

\subsubsection{User data}
As mentioned in Section \ref{section:token} the application will request a token on first launch, which will be used to identify the device to the web service for storing user preferences. This token must be stored between launches, so it is stored in the device Keychain. The Keychain is a key-value store provided by iOS for the storage of credentials, where access to stored items is sandboxed between applications. If the user has iCloud Keychain enabled their keychain is synchronised between their devices transparently by the OS, so a user with multiple devices will be represented as one device in our web service.

In order to access the Keychain, we use the Latch library\cite{Latch} which provides a wrapper around the iOS Keychain API. On each launch the application loads the Keychain token value into a global variable, so that it can be accessed with little overhead by the rest of the application.

\subsection{Deployment}
For the mobile application there are a number of options available for deploying prerelease applications to devices. Offerings such as TestFlight and HockeyApp allow developers to invite users to a test group and then act as a effectively private App Store to install the application on devices. However in the case of TestFlight the application is reviewed prior to deployment, which can cause delays. The other option is to deploy directly to the devices by tethering them to a computer running the Xcode IDE. Due to the timeframe of this project and the small scale of the evaluation, tethering was the approach used in this case. For a further round of evaluation, TestFlight could easily be used to carry out a diary study.
