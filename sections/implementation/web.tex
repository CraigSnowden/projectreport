\section{Web application}
\label{chapter:web}

The web application is part of the same Ruby on Rails application as the web service, utilising the ability to respond to a request with different content types based on what the client can accept. Thus we can use the same code that responds to the mobile application's requests in a desktop environment.

The web pages use the Bootstrap CSS framework to reduce the need for custom styling and provide a clean UI on the desktop.

\subsection{Accessibility}
In order to allow users with screen readers to easily access the application, a number of standard accessible web development practices were used.

\subsubsection*{Form labels}
Clearly in any application, useful labels for form fields are important, however a screen reader will particularly benefit from the use of the `label[for]' attribute. This allows the screen reader to determine what label is referring to a particular field.
\subsubsection*{Table header scopes}
In order for a screen reader to express to its user what the content of a particular table field means in the context of the table overall we must use the `th[scope]' attribute to specify whether the header field refers to the column or row it is adjacent to.
\subsubsection*{Skip to content}
Often web applications will have a large amount of auxiliary content that precedes the main content of the page, e.g. navigation bars and branded headers. In order to allow the user to start reading the content directly, we must provide a link at the start of the page body that moves the browser focus to the start of the main content. This link can be hidden to non screen reader users so that it does not affect the visual flow of the page.

We can augment this on search results pages by providing a further hidden link before search forms that skips straight to the results, in order to avoid having to step over the form again unnecessarily.
\subsubsection*{ARIA Autocomplete Notifications}
Fields which use autocomplete depend on the visual indication that results have been loaded, in order to notify the user that there are results available to select. These page content changes are not normally given to the screen reader. In order to make this more clear, the \ac{ARIA} initiative created the `aria-autocomplete' text field attribute, which allows the developer to specify how they will display suggestions to the user. This attribute is not implemented by the default Google Places Autocomplete JavaScript library used in this application, so the library had to be patched to include this attribute.

\subsection{Deployment}
In order to be accessed on the web, the application must be deployed to a publicly accessible server. There are a number of approaches available for deploying Ruby on Rails applications, which depend on where the application is to be hosted. A common approach is to use a \ac{PaaS} offering, such as Heroku. However these services are often expensive, and would not allow the batch processing required to do the regular import from the Traveline dataset.

A more flexible approach is to deploy to a server directly. This does not need to be a physical machine, and indeed a virtual machine was used during development. In order to make the deploy process consistent, the Capistrano\cite{Capistrano} tool was used, which provides a \ac{DSL} for running commands on remote servers. By using Capistrano we can automate the processes required to upload code to the remote server, modify the schema of the database and seamlessly restart the application workers that serve the web service requests. A scheduled task can be used to download and ingest the Traveline data, which can be managed with Capistrano using the Whenever library\cite{Whenever}.
