
\section{Identifying functionality}

Given the wishlist we had identified through consulting with a user and the prior research, a list of potential functionality for an accessible transit application was developed and prioritised.

\begin{enumerate}
	\item National dataset
    \item Planning in advance through a desktop UI
    \item Ability to store and recall favourite items
    \item Ability to locate the user, without mandatory GPS location
    \item Offering of turn by turn directions
    \item Mobile training of place name pronunciations
    \item Crowd sourcing of landmarks and objects near stops
\end{enumerate}

Clearly this feature list is extensive, so it was decided to concentrate on functions 1-4. As mentioned previously, tasks requiring human intelligence such as 6 \& 7 have been subject to prior research \cite{Campbell2014} and could form part of a future project in this area.


\section{Architecture}
\label{chapter:architecture}
\begin{figure}[ht]
\centering
\includegraphics[width=\textwidth]{images/Talking_Buses_High_Level.pdf}
\caption{High Level Architecture Diagram}
    \label{fig:high-level}
\end{figure}
Breaking these functions into a high level architecture, as pictured in Figure \ref{fig:high-level} we see that there are four aspects of the system.

The main source of this data will be from the Traveline National Dataset. While this data lacks the ease of access of a transport provider specific API, the schema offers the flexibility of being able to parse each file in an application specific way and query it as necessary. The creation of the parser for this project is discussed in Section \ref{chapter:parser}.

This static data can be augmented by using the real time data provided by TransportAPI.com, a commercial provider with a free offering, allowing us to give live departure times when available from the transport operator.

As the data is from multiple sources, and user's preferences are recorded alongside this data, a web service is required (Section \ref{chapter:api}) that mediates the access of the respective client applications to the transport data and stores the user preferences generated through using the applications.

The client applications will represent this data to the user, and will be available on two platforms: iOS (Section \ref{chapter:ios}) and the Web (Section \ref{chapter:web}).

\section{Defining functional requirements}
		\paragraph*{Parser}
		\begin{enumerate}
			\item Parse the Traveline XML
			\item Populate the database
		\end{enumerate}

    \paragraph*{API}
    \begin{enumerate}
        \item Expose Traveline data
				\item Reconcile real time data from TransportAPI
        \item Store user preferences
    \end{enumerate}

    \paragraph*{Pre-planning interface}
    \begin{enumerate}
        \item Authenticate the user, linking them to their mobile device
        \item Look up stops using place names
        \item Look up routes using their route name and the locality in which they operate
        \item Store favourite stops and routes
    \end{enumerate}

    \paragraph*{Mobile application}
    \begin{enumerate}
        \item Provide means for the user to identify their mobile device on their desktop
        \item Look up stops using place names and current location if desired
        \item Look up routes using their route name and the locality in which they operate
        \item Store and recall favourite stops and routes
    \end{enumerate}

\section{Use cases}
\label{sec:use-cases}
As an example of a scenario in which this system and its pre-planning approach can help, here I will demonstrate how a user can complete a task with the completed system. The task given here is later used for evaluation with users (Chapter \ref{chapter:eval}).

\begin{displayquote}
	``On the web, find the departures for the Sciennes House Place bus stop and save it to your phone.''
\end{displayquote}

When a user wishes to save a stop as a favourite, they simply visit the Talking Buses web application on their desktop (Figure \ref{fig:device-link}). They first need to link the application to their mobile device using a shortcode, which they can access from the `Cloud Sync' tab (Figure \ref{fig:device-code}). Whenever they need to log in to the web application they can reaccess this code.

Having logged in, the user can see any favourites they have stored so far (Figure \ref{fig:device-listing}). They can then navigate to Stop Search on the top navigation bar, where they can search for this stop with autocomplete (Figure \ref{fig:search}).

The results of their search are presented as a table, with the services that visit that stop (Figure \ref{fig:search-results}). The user can now save this stop to their favourites, or view the departures from this stop.

Now that the stop has been saved to their favourites, the user can navigate to the favourites list (Figure \ref{fig:stops}) and see that the stop has been saved (Figure \ref{fig:favourite-stops}). The user can then view the live departures from this stop (Figure \ref{fig:departures}).

% \begin{landscape}
\begin{sidewaysfigure}[H]
    \centering
    \begin{subfigure}[b]{0.47\textwidth}
				\centering
        \fbox{\includegraphics[width=\textwidth]{images/use-case/1}}
        \caption{\textbf{Web:} First we link with our mobile device}
        \label{fig:device-link}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.47\textwidth}
				\centering
      	\fbox{\includegraphics[width=\textwidth]{images/use-case/2}}
        \caption{\textbf{Phone:} ... to do this we need a short code}
        \label{fig:device-code}
    \end{subfigure}

    \begin{subfigure}[b]{0.47\textwidth}
				\centering
        \fbox{\includegraphics[width=\textwidth]{images/use-case/3}}
        \caption{\textbf{Web:} Now we have logged in, we can see our favourites}
        \label{fig:device-listing}
    \end{subfigure}
		~
		\begin{subfigure}[b]{0.47\textwidth}
				\centering
        \fbox{\includegraphics[width=\textwidth]{images/use-case/4}}
        \caption{\textbf{Web:} We can search for our stop}
        \label{fig:search}
    \end{subfigure}
    \caption{Demonstration of a use case}
		\label{fig:usecase}
\end{sidewaysfigure}
% \end{landscape}
\begin{sidewaysfigure}[H]
	\ContinuedFloat
    \centering
    \begin{subfigure}[b]{0.47\textwidth}
				\centering
        \fbox{\includegraphics[width=\textwidth]{images/use-case/5}}
        \caption{\textbf{Web:} ...and save it as a favourite from the results}
        \label{fig:search-results}
    \end{subfigure}
    ~
    \begin{subfigure}[b]{0.47\textwidth}
				\centering
      	\fbox{\includegraphics[width=\textwidth]{images/use-case/6}}
        \caption{\textbf{Phone:} Now we can navigate to the favourites list}
        \label{fig:stops}
    \end{subfigure}

    \begin{subfigure}[b]{0.47\textwidth}
				\centering
        \fbox{\includegraphics[width=\textwidth]{images/use-case/7}}
        \caption{\textbf{Phone:} The stop we just saved is there}
        \label{fig:favourite-stops}
    \end{subfigure}
		~
		\begin{subfigure}[b]{0.47\textwidth}
				\centering
        \fbox{\includegraphics[width=\textwidth]{images/use-case/8}}
        \caption{\textbf{Phone:} We can now see the departures from this saved stop}
        \label{fig:departures}
    \end{subfigure}
		\caption{Demonstration of a use case}
		\label{fig:usecase}
\end{sidewaysfigure}
