\section{Parser}
\label{chapter:parser}

Data from public transport providers is available from multiple sources. One option is directly from the operators, which in the case of Transport for Edinburgh is available through a comprehensive web service with access to real time data. However not all operators are as open with their data, and indeed there is no consistency in the APIs that they implement.

National data is available from the Traveline government agency, with their Traveline National Data Set \cite{TravelineData}. This data is freely available on request, with a closed FTP server providing weekly data dumps from their timetabling service. These dumps take the form of a ZIP file per region\footnote{East Anglia, East Midlands, London, North East \& Cumbria, North West, Scotland, South East, South West, Wales, West Midlands and Yorkshire} with each bus service within the region being specified with an XML file complying with the TransXchange schema.

The TransXchange schema is defined by a large document\cite{DepartmentforTransport2010}, however the data source being used for this project uses a very small subset of this schema. A truncated example of a service specified in TransXchange can be found in Appendix \ref{appendix:txc}. The main role of this parser is to provide a locally accessible cache for data that is not real-time -- e.g. the stops a particular bus route visits. This data can be reconciled with the commercial Transport API \cite{TransportAPI} to give more comprehensive information about the available bus routes.

In order to process the large volume of TransXchange data released weekly, some degree of concurrency is required to maximise performance. This is possible as each TransXchange bus service specification can be considered as a self-contained unit. I utilised the Sidekiq\cite{Sidekiq} job scheduler as this can be integrated into the wider Rails application and use the existing database modelling (described in Chapter \ref{chapter:api}).

In Sidekiq, worker threads perform tasks from a queue held in a Redis\cite{Redis} store. These workers load the queued TransXchange file, parse it and populate the database through the process depicted in Figure \ref{fig:worker}.

\begin{figure}[ht]
  \centering
  \caption{Worker flowchart}
  \label{fig:worker}
  \begin{tikzpicture}[node distance=2cm]
    \node (start) [startstop] {Start working};
    \node (taskavail) [decision, below of=start, yshift=-1cm] {File to process?};
    \node (wait) [process, right of=taskavail, xshift=2cm] {Wait};
    \node (parsexml) [io, below of=taskavail, yshift=-1cm] {Parse XML};
    \node (populatedb) [process, below of=parsexml] {Populate Database};

    \draw [arrow] (start) -- (taskavail);
    \draw [arrow] (taskavail) -- node[anchor=east] {yes} (parsexml);
    \draw [arrow] (taskavail) -- node[anchor=south] {no} ([xshift=2cm]wait);
    \draw [arrow] (wait) |- (taskavail.north);
    \draw [arrow] (parsexml) -- (populatedb);
    \draw [arrow] (populatedb) -| ([xshift=-2cm]parsexml.south west) |- (taskavail);
  \end{tikzpicture}
\end{figure}

For the parsing itself the Nokogiri XML library was used, as this offers easy DOM parsing of reasonably sized documents. It was noted that DOM parsing is slower and less performant than SAX parsing, however the batch processing nature of this particular project means that the parsing process need only take reasonably short time.

The Nokogiri library offers XPath and CSS selector based DOM searching. The latter was chosen as it offered a more familiar and clean looking method of searching, with little overhead over XPath\cite{SO}.
