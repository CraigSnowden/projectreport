\section{Web Service}
\label{chapter:api}

The web service has two functions: to provide data that was ingested by the parser to the client devices over an API, and to store the user's preferences.

\subsection{Choice of framework}

The choice of web application languages and frameworks is more down to personal opinion. I chose to use Ruby and Ruby on Rails as the core functionality covers a number of the basic requirements.

\begin{itemize}
    \item Powerful \ac{ORM} with ability to query with raw SQL if required
    \item \ac{DSL} for specifying JSON responses
    \item Wide array of libraries
\end{itemize}

Lighter weight Ruby web frameworks are available, including Sinatra, however there is no included \ac{ORM} and often libraries are designed for the Rails framework.

\subsubsection{Design pattern}
\label{sec:mvc}
Rails uses the Model View Controller pattern, which promotes code reuse by separating the code that interacts with data from the code that interacts with the user (depicted in Figure \ref{fig:mvc}). In order to implement different user interfaces, the view and controller components can be swapped out without affecting the data model.

\begin{figure}[ht]
  \centering
  \includegraphics[height=7cm]{images/mvc.png}
  \caption{A diagram of the interactions between the components of the MVC pattern and the user. (Source: Wikimedia Commons)}
  \label{fig:mvc}
\end{figure}

The view components are the presentation layer of the application, which adapts the data for the particular output template (this could be HTML, JSON, XML or another format). The model components act as the persistance layer for the data, which utilise the \ac{ORM} to map tables in the database to objects in Ruby.

\subsubsection{Database}
There are a number of relational and NoSQL databases available that would be suitable for the persistence layer of this application. At the early stage of this project I considered using a NoSQL store for the parsed Traveline data, however there was difficulty in querying as documents\footnote{the NoSQL ``record'' equivalent in relational parlance} are considered to be individual units of information. As each bus service specification contains references to the stops it visits, being able to access the services that visited a particular stop was unduly difficult. This sort of query is trivial in a relational database, which made the decision to use a relational database obvious.

In the relational space, there are a number of offerings that do mostly the same job; PostgreSQL, MySQL, SQLite, etc. MySQL was chosen as it was already available on the server I was deploying to, however the Rails \ac{ORM} offers a number of adapters to interface with other databases, so switching would not be difficult.

\subsection{Design}
\subsubsection{Authentication approaches}
\label{sec:authentication}
As we need to store user preferences in order to allow the synchronisation of pre-planning information, we need a way to identify a particular user. There are a number of different approaches which are used in these scenarios, which have their advantages and drawbacks.

\paragraph*{Username and password}
This is a traditional approach which requires the user to sign up to the service and use a user account which we store in a database. This is ideal for applications that wish to allow the user to log in to multiple devices, however has the obvious drawback of requiring the user to create (and remember) a username and password. A less obvious drawback is the need to store these passwords in a secure fashion, utilising hashing algorithms.

\paragraph*{Single sign on}
A popular approach with many web services is to use the authentication provided by another vendor, such as Google, Twitter or Facebook. This is ideal for users who already have accounts with these other services, however might require a fallback approach of username and password for users who do not subscribe to these services. Additionally the processes to authenticate with these other services can be complicated, especially on mobile platforms.

\paragraph*{Token} \label{section:token}
When authenticating a mobile device with a server, we can generate a token string that is used in subsequent interactions. This token can then be used to identify the particular mobile device to a web service. A short version of the token can be generated when necessary, and provided to the user to allow them to identify their mobile device to another system.

Generating an \ac{UUID} when the mobile application is first launched and cycling a short alphanumeric code on each launch would allow the user to identify their mobile device to the pre-planning system when they need to synchronise user data. When the user gives the pre-planning system their current ``short code'' this can be referenced against the particular device token and stored in their browser session. When this session expires, the user can simply reauthenticate by accessing a new code from the mobile application.

The downside of this approach is that a user is tied to a particular device, and will be considered a new user when they change device. However, this approach does not involve remembering any details on the user's part and is therefore a good fit for this particular application.

\subsection{Implementation}
\subsubsection{Data models}
\begin{figure}[h]
  \centering
  \includegraphics[height=10cm]{images/erd.pdf}
  \caption{Entity Relationship Diagram}
  \label{fig:erd}
\end{figure}

The entities being stored in the database mentioned above are depicted in Figure \ref{fig:erd}. These entities were chosen to store both the output from the parser (Section \ref{chapter:parser}) and the user preferences generated by the client applications. These entities represent the persistence layer of the Web Service, or the Model component of the MVC Design Pattern (Section \ref{sec:mvc}).

\paragraph*{Stop}
This entity is populated with the national dataset of bus stops \cite{TravelineData}. The primary key is the `ATCO Code', which is an identifier used in both the Traveline dataset and the TransportAPI service.

\paragraph*{Service}
This entity is created on the import of a service from the Traveline Dataset. In TransXchange, the  number that would commonly appear on the front of a bus (e.g. 49, X42, 100) is referred to as the `line'.

\paragraph*{StopVisit}
When importing the service, the stops that that particular service visits are matched to the Stop entity. This is used to give service listings against stops on the client-side.

\paragraph*{Device}
When the mobile application is first launched, a \ac{UUID} is generated and stored here. This entity is then used to store user preferences.

\paragraph*{SavedStop}
The user is able to save stops, which are stored here with reference to both their device and the particular stop entity they wish to save.

\paragraph*{Trip}
When storing a trip, the initial and destination locations are stored as [longitude, latitude] pairs so that `fresh' direction results can be generated at the time of need.

\subsubsection{API endpoints}
The API endpoints were designed to allow the same URLs to be used for both HTML (web application) and JSON (mobile app) representations, this is important because it ensures that behaviour can be mostly consistent between the two applications with the differentiation being at the presentation layer. Rails has this built in, basing the response type on the Content-Type requested in the HTTP header.

The JSON output of the API is modelled using the JBuilder\cite{JBuilder} \ac{DSL} allowing the responses from the API to contain everything that the mobile application requires in only one request. Reducing the number of requests that the mobile app needs to make is particularly important when the connection is over a cellular network, as there is significant overhead in completing a HTTP request.

The API can be broken down into a number of resources, which deal with different database entities.

\begin{table}[ht]
\caption{API Entities}
\label{api-entities}
\begin{tabular}{| p{0.15\textwidth} | p{0.5\textwidth} | p{0.35\textwidth} |}
\hline
Entity & Description & Endpoints \\
\hline
/stops &
  Entities relating to stops (points of access defined in the NAPTAN gazetteer) &
  /near

    /services

  /[ATCO code] \\
    \hline

/services &
  Bus routes &
    /near

    /stops/[Service id]\\

\hline

/directions &
  Point to point guidance &
    /acquire\\

\hline

/devices &
  Devices &
    /my\_device

    /link

    /[Device token]

    /[Device token]/cycle\_code

    /[Device token]/save\_stop

    /[Device token]/remove\_stop

    /[Device token]/save\_trip \\
\hline
\end{tabular}
\end{table}

\subsubsection{Development process}
Initial development of the web service was completed alongside the parser, in order to get the Traveline dataset into a queryable state. Subsequent development was completed in an iterative approach, alongside the client applications.

Testing of the JSON API endpoints was completed using the Postman HTTP Client tool, which allows HTTP requests to be made and the response inspected. Packet dumps using the Wireshark tool were used to ensure that the correct requests were being made from the iPhone client.
