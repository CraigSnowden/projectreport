\chapter{Evaluation}
\label{chapter:eval}
\section{Methodology}
 Evaluation was performed using the ``think aloud'' methodology, described in the Carnegie Mellon University ``User-Centered Design and Testing'' course notes\cite{iCarnegie2003}. In a think aloud evaluation the evaluator is interested in what the participants think about as they perform tasks. Evaluation dialogues were scripted in advance, with printed tasks supplied to the evaluating users (Appendix \ref{chapter:evalmaterial}).

Users were asked to perform three tasks that I anticipated would be commonly performed by a user who was pre-planning a day out. Two of the three tasks involved using the mobile-web synchronisation feature (\textbf{T1}, \textbf{T3}).

As an application using a multi-platform approach to facilitate pre-planning, it is important to ensure that the authentication aspect of the application (that is, using a generated code from mobile to authenticate on the web) is intuitive to any user -- whether they are using a screen reader or not.

\begin{enumerate}[label=\textbf{T\arabic*}]
  \item \emph{On the web, find the departures for the Sciennes House Place bus stop and save it to your phone.}

    Firstly this tests whether the user is able to link to the mobile device, and then carry out a basic search operation. This is the use case described in Section \ref{sec:use-cases}.
  \item \emph{Find out where the Lothian Buses 100 goes.}

  This query is carried out entirely on the phone. This tests the autocomplete feature in a mobile context.
  \item \emph{Find directions from Sciennes to Edinburgh Zoo and save them for later.}

  This task replicates a real world use case, where the user is planning ahead for a day out.
\end{enumerate}

\section{Procedure}
Users were given a iPhone 6S Plus with the Talking Buses software installed and a laptop with the web application loaded. Actions performed by the user were recorded using the built in screen recording software for both platforms, with audio being recorded separately for future reference. Server logs for user evaluation sessions were stored in order to allow for more automated analysis if necessary. During the session, I solely took notes of what the user was doing and did not answer questions except in the event that the user was unable to continue (they had become lost, or a technical failure had resulted).

Four participants were recruited to participate in an evaluation session. These participants were experienced in using mobile devices and the web, however none currently utilised bus transportation regularly.

Participants were asked to talk through their thoughts and actions while they used the application for the first time. ``Thinking aloud'' gives an insight into points of confusion that would perhaps be unclear through other usability evaluation methods, such as a diary study, and are possible with little time commitment from the study participant. The evaluations were carried out in a quiet area using provided hardware, which perhaps does not provide much insight  into the contexts in which the application might be used in a production environment.

\section{Results}
Having carried out these evaluations, I could extract the critical events from the notes to determine where the problems were. Solutions to these are discussed in Section \ref{sec:iteration}.

\begin{landscape}
\begin{longtable}{| b{2cm} | b{6cm} | b{6cm} | b{6cm} |}

  \hline
   \textbf{Participant} & \textbf{T1}                   & \textbf{T2}      & \textbf{T3}                                    \\
   \hline
   \endfirsthead
   \hline
   \textbf{Participant} & \textbf{T1}                   & \textbf{T2}      & \textbf{T3}                                    \\
   \hline
   \endhead
   \hline
   \endfoot
   \hline
   \endlastfoot

\hline

\hline
\textbf{P1} &
  \begin{itemize}
    \item Started on phone rather than web - confusion with task
    \item Went to routes rather than stops
    \item Did not save stop
  \end{itemize}

  Did not complete successfully &
  \begin{itemize}
    \item Didn't understand why location was required
    \item Placeholder text in field was confusing
  \end{itemize}

  Completed successfully &
  \begin{itemize}
    \item Was unable to exit choose initial location field
    \item Started on phone, so was unable to save
    \item No visual indication of the location having been chosen
  \end{itemize}

  Completed successfully \\
\hline
  \textbf{P2} &
    \begin{itemize}
      \item Confusion with difference between opp/adj (used to differentiate between stops on both sides of a street)
      \item Can't save on the stop departures page
      \item What is the short code?
    \end{itemize}

    Completed successfully &
    \begin{itemize}
      \item Didn't understand why location was required
    \end{itemize}

    Completed successfully &
    \begin{itemize}
      \item Started on phone, can't save there
      \item Browser session had disappeared, required linking to the device again
    \end{itemize}

    Did not complete successfully \\

\hline
  \textbf{P3} &
    \begin{itemize}
      \item Started on phone rather than web - confusion with task
      \item What is a route?
    \end{itemize}

    Completed successfully &
    \begin{itemize}
      \item Couldn't find this on the web
      \item Didn't understand why location was required
    \end{itemize}

    Completed successfully &
    \begin{itemize}
      \item When you save to a device, the trip is reloaded which takes a long time
    \end{itemize}

    Completed successfully \\

\hline
  \textbf{P4} &
    \begin{itemize}
      \item Why do places outside the UK appear in autocomplete?
      \item Can't save on the stop departures page
      \item What is the short code?
    \end{itemize}

    Completed successfully &
    \begin{itemize}
      \item Placeholder text in field was confusing
      \item Didn't understand why location was required
    \end{itemize}

    Completed successfully &
    \begin{itemize}
      \item No visual feedback when the trip has been saved
    \end{itemize}

    Completed successfully \\

\end{longtable}

\end{landscape}

\section{Limitations}
Due to time constraints, I was unable to arrange a further meeting with the partially sighted user John. Thus the application, and its authentication model, would be tested on sighted users as they were more readily available.

For the same reason, a longer term diary study would be difficult to complete and analyse for any useful outcome. For future development of this project, a longer term study with screen reader users would be essential.

\section{Iteration}
\label{sec:iteration}
Issues identified during the evaluation process were recorded, and resolved if trivial.

\subsection*{Lack of documentation}
Despite users being able to complete the tasks, there was clearly confusion as to how to complete the device linking process (\textbf{P2}, \textbf{P4}). The version of the web interface evaluated did not provide any help to the user as to how they would acquire the required information. This is certainly important, however users were able to resolve their confusion by looking at the more thorough documentation available on the mobile application.

All participants were confused as to why location was required to search for a bus route, which could be resolved by providing further documentation in both applications, through providing a help button which links to context sensitive documentation. Non-accessible applications might use tooltips or popover style, however this is recommended against in the BBC Mobile Accessibility Guidelines \cite{BBCMobile} as the ARIA tooltip property does not have complete platform adoption.

Another approach used by the Transport for Edinburgh accessible mobile application is prompt the user with modal dialogs when they first complete an action. This is not ideal as it does not permit the user to access the help again, and does not provide the space needed to give detailed assistance. Full documentation on the web can be linked to in mobile, to provide a richer experience for users who prefer to access longer content on their desktop.

\subsection*{Keyboard trap}
`Keyboard trap' \cite{BBCMobile} refers to an issue with a mobile text input, where the user is unable to dismiss the keyboard. Evaluation revealed (\textbf{P1}) that this was possible within two contexts in the application: input of route numbers and input of route locality. This occurred because the form was not resigning focus when the user tapped off the form. The resolution was to modify the affected view controller to provide a dismiss button on the keyboard.

\subsection*{Missing save button}
On the web application when viewing the departures leaving from a particular stop, it was not possible to save stops to your device (\textbf{P2}, \textbf{P4}). Instead the user had to go back to the search results table and select save from there. As this was not particularly helpful, the save button is now available on the departures page.

\subsection*{Loading indication}
Many tasks in the application require communication with the network, which can take a long time particularly over a slower cellular connection. For finding directions between two points the delay is significant, as it requires communication with Traveline and our own API. This delay can cause confusion to the user (\textbf{P1}, \textbf{P4}), as it is unclear whether the application is responding.

In order to reassure the user, it is useful to provide an indication that there is network activity ongoing. A typical way to do this would be through using an animated spinner, however this would be less useful for the target audience. The iOS accessibility API provides methods for giving audio notifications, which we now use to alert the user when loading begins and completes.

\section{Proposed further modifications}

\subsection*{Mobile-Web feature mismatch}
Due to time limitations, it is not possible to search for a bus route on the web application. When given the task for search for routes this lead to some confusion (\textbf{P3}).

One user thought the feature was unnecessary (\textbf{P4}), which may require further evaluation. The term `route' itself was considered confusing (\textbf{P3}), as the word is often used to refer to the functionality we refer to as `directions'. The alternative term `service` may be considered less confusing.
