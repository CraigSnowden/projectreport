\section{Background}
\label{chapter:background}
Improving access to public transport using mobile technology has been the subject of some research. Unfortunately this type of project is hard to complete to a publicly deployable state, as a large proportion of these projects require installing devices at stops or on buses.

\subsection{Communicating with drivers}
\label{sec:communication}
Blind travellers often find communicating with a bus driver complicated, as drivers are often not aware of their specific needs. The system described in \cite{Markiewicz2010} utilised a Java application installed on basic handsets and a pilot program involving Personal Digital Assistants installed on buses to facilitate communication with the driver. The traveller is able to specify the bus they are waiting for, with the driver informed through a tablet of any waiting disabled passengers at their upcoming stops. The passenger is also provided with voice based alerts to notify them about upcoming buses. However due to the need to install hardware and infrastructure on buses, this method is unlikely to scale to nationwide deployment. \cite{Azenkot2011} discusses the use of Special Assistance cards to communicate with the driver. Locally, the South Eastern Scotland Transport Agency utilise the Thistle Assistance Card (Figure \ref{fig:thistle}) to provide a standardised means of communicating accessibility information.

\begin{figure}[ht]
\centering
\includegraphics[height=8cm]{images/thistle-card.jpg}
\caption{SEStrans Thistle Assistance Card}
    \label{fig:thistle}
\end{figure}

\subsection{Navigating}

Focussing on the problems of finding a stop, another project \cite{Campbell2014} utilised crowd sourcing to provide richer information about the bus stops in the Washington area in order to augment the public data available. Utilising the Amazon Mechanical Turk, information about physical features near the bus stops was gathered and provided to users with an iOS application. Data from the One Bus Away live bus information service was also used to allow some element of planning.  The provision of data of this type is limited to small areas due to the necessity of humans in the data capture process. The crowd sourcing approach was also used to create an Android braille display supporting application \cite{Azenkot2011}, predating the built in support for braille in Android.

Looking at identifying a bus arrival for sighted users, a project in Helsinki \cite{Kostiainen2011} made use of auditory cues to indicate the arrival of buses at a stop. They indicated that these non-speech alerts could allow users to place more focus on other tasks, however did not review the efficacy for blind and partially sighted users. As blind users will depend on sound in order to determine what is going on around them, perhaps the introduction of additional non-speech sound could be more distracting than useful.

\subsection{Use of mobile devices}
\emph{Freedom to Roam} \cite{Kane2009} looks at the various uses and strategies associated with mobile devices through a diary study of 19 people with sight loss. The study found that when visiting a new place, participants would do their planning before leaving the home as the information would not be as accessible when on the move. Participants found their devices useful as a ``safety blanket", and would configure them to suit their individual needs, utilising the adjustable text sizing, screen reader and magnification capabilities where available.

The availability and use of screen reading software on smartphones has become more apparent to the general population through high profile blind users such as Stevie Wonder, featured most recently in the 2015 Apple Holiday advertisement. A staunch supporter of Apple's commitment to accessibility, Wonder can be seen using the VoiceOver screen reading software's speech and braille output in television appearances \cite{Corden2015}. Android users also have access to a built in screen reader with TalkBack introduced in version 1.5. From the perspective of the developer, both platforms provide Accessibility APIs that allow user interface elements to be augmented with descriptions available to screen reader users that provide contextual information normally given by visual cues. Unfortunately this additional work to describe their UI is not often performed by developers, making some applications completely unusable by screen reader users.

\begin{figure}[ht]
\centering
\includegraphics[height=10cm]{images/tfe.PNG}
\caption{Transport for Edinburgh ``TfE Talk''}
    \label{fig:tfe_talk}
\end{figure}

\subsection{Public transport organisation responsibilities}
Public transit agencies and companies share a responsibility to improve their provision of service for users with additional needs -- however this work very much depends on the initiative of the agency involved. Lothian Buses, the largest local bus company in the United Kingdom, provide an iOS application (Figure \ref{fig:tfe_talk}) with an alternative mode for blind users developed in association with the Edinburgh Royal Blind School. In creating this partition however the effort of development is increased, with functionality improvements having to take place on both ``sides" of the application. This is less common, with Apple recommending that accessibility is incorporated into the regular application \cite{Experience2011}.

The \acf{RNIB} has created a Bus Charter \cite{RNIB2014} to campaign for the removal of barriers related to independent travel on buses.

\begin{displayquote}
6. We will review our timetable and bus stop information in conjunction with blind and
partially sighted people, local authorities and other stakeholders to ensure it is as
accessible as possible.

11. We will explore all options for providing audio announcements on our buses,
including making use of new technologies when they become available.
\end{displayquote}

\subsection{Conclusion}

While steps have been made to improve the accessibility of public transport to blind and partially sighted people, these efforts have not lead to improvements on a national scale. Many of the solutions that have been presented depend on the work of particular transport operators or local governments to put in place the infrastructure required to support the blind user base, and while these are often legally mandated, full adoption has not been seen so far.

On the other hand, mobile devices are being adopted by blind people, who are seeing the benefits of carrying a mobile device as a safety blanket. Mobile devices could therefore be used to satisfy obligations to provide accessible information, such as those found in the RNIB Bus Charter mentioned above.
