\chapter{Requirements Gathering}
\label{chapter:requirements}
\section{Methodology}
When devising the interview questions and structure, \cite{Adler1995} was used for guidance. In order to gain a better understanding of the users' requirements, I planned a semi-structured interview in which questions would be used to introduce themes and prompt discussion. The questions used can be found in Appendix \ref{appendix:interview}.

I was interested in getting a user's perspective on the issues that they face with using public transport and the solutions that already exist. I identified three themes of discussion:
\begin{itemize}
  \item Finding a bus stop
  \item Identifying the correct bus
  \item Planning a longer journey
\end{itemize}

Through contacting \ac{RNIB}, I arranged an interview with a blind public transport user who for the purpose of this study will be called John. John is over 50 years old, lost his sight in his twenties, and currently retains perception of large changes in light level. John is a frequent public transport user, making use of local buses and trains for longer journeys. John has also worked extensively with RNIB to promote their schemes to improve accessibility, and was part of the group who introduced the Thistle Assistance Card (Section \ref{sec:communication}).

I, as primary researcher, lead the interview and devised the starting questions. I was accompanied by a note taker, who would allow me to better engage with the interviewee, and the interview was audio recorded for future reference. We met in Cafe Tiki at RNIB Edinburgh, a location familiar to John.

\section{Interview}
% Who is this guy

As found in previous research, finding stops is an difficult issue for blind transport users. John discussed utilising the difference in light created by large immobile objects such a bus shelters, and physical feedback from a cane for street furniture such as bins. The direction of travel indicated by the sound of road traffic was a useful method for building up a mental picture of the landscape. In an unfamiliar location, help from sighted pedestrians is often sought to ensure that the correct stop has been reached. Accessibility specific hardware, such as the Trekker Breeze voice based navigation device are often used by blind travellers, however the expense associated with these single use devices makes smartphones a far more compelling option. RNIB provide a navigation application that provides information on the location of traffic lights and phone boxes that could be useful in these scenarios.

Having found the stop, communication with the bus driver was a significant component in identifying the correct bus. Partially sighted travellers may be able to see the large bus number, however users with less sight will depend on the driver communicating with them. Drivers are increasingly being provided training to notice users with additional needs and provide appropriate support, however this is not universal and therefore cannot always be depended on.

Once the correct bus has been identified, John will also depend on the driver notifying them of the correct stop at which to get off. This too is not completely dependable, as routes can be diverted and drivers can change midroute. John suggested that it may be comforting to match their current location with the correct route, to ensure that the correct bus was boarded. Clearly this would not work in the case of route diversions.

In order to improve the service offered by bus drivers to blind users, RNIB offers ``swap sessions'' where the drivers get to experience navigating as a blind user. These sessions were useful, but have not been universally adopted.

He showed us a specific public transport application that he utilised regularly to find train times - UK Train Times from Agant Ltd \cite{Agant}. He specifically identified the Next Train Home and Favourite Stops features as particularly useful.

The GPS tracker was one of the concerns identified by the user due to the battery life implication.

He had concerns that the data offered by Traveline was not particularly current, as it depended on the accurate reports of the local bus operators. In his local area, the operators of particular services changed relatively frequently, so information was often stale and required direct contact with the operator by phone. Information was often available in accessible formats such as Braille, but he was not confident in his ability to read it.


\begin{figure}[ht]
\centering
\includegraphics[height=9cm]{images/user-devices.jpg}
\caption{John with his mobile devices}
    \label{fig:devices}
\end{figure}

John showed us how he uses the VoiceOver screen reader. He utilised the Mobience RiVo Bluetooth keypad (pictured in John's hands in Figure \ref{fig:devices}) to speed up operation. This Korean device offers a ``small QWERTY" text input option offering physical feedback that the `hunt and peck' method on the software keyboard cannot provide. The time associated with inputting text would be a challenge when on the go, as it would not be possible while moving. The RiVo device also allows control of the screen reader, meaning the phone can be operated while in a pocket alongside bone conducting headphones that do not inhibit hearing the sound of the environment. For sit-down sessions, a full QWERTY Bluetooth keyboard can be paired with the phone to further ease the input of long text.

The VoiceOver software itself can be hard to learn, often requires the assistance of a sighted person to set up, and can be frustrating to the stage that many people can give up. However, once the gestures have been learned, reference points can be obtained that increase the speed of finding particular items. Particular gestures such as swiping to navigate and double tap to select mean that complete precision is not required, and the ``rotor'' method of changing settings mean that the speech rate can be adjusted to assist usage in loud environments.

Despite the iPhone offering a good experience on the go, John preferred using the screen reader on his PC: JAWS \cite{JAWS}. A commercial application, it offers more customisation options than VoiceOver, allowing John to correct pronunciation errors with a more realistic voice. In order to plan trips, he makes use of the Traveline website to get directions and timetables ahead of time. Once these have been acquired, Traveline does offer calendar export of trips, however there is no facility for synchronising these to a mobile device for later use.

Synchronisation of trips was suggested by John as a possible avenue of improvement, as this would reduce the effect of the poor text entry and pronunciation available on the go.



\section{Wishlist}

After meeting with John, we were able to develop a ``wishlist'' of features that an ideal accessible public transport application would have. A subset of these features form the basis of this project.

\subsection*{Data for all regions}
As discussed earlier, public transport providers create their own accessible applications which offer data about their services. An ideal application would make use of a national dataset in order to accommodate users in multiple regions and those who use multiple bus operators.

\subsection*{Pre-planning}
The current applications that exist on the web and mobile do not offer the ability to synchronise between the platforms. By utilising a web service both platforms could share user data and allow users to plan their trips on the platform on which they are most comfortable.

\subsection*{Mobile screenreader training}
Screen readers depend on Text To Speech technology that is not necessarily aware of regional place names, and therefore confusion can be caused when a place name is read incorrectly. Desktop screen readers allow the user to ``train'' the synthesiser to correct mispronunciations. By using phonetic spelling and potentially crowdsourcing to share workload, this could be offered on mobile platforms too.

\subsection*{Identifying physical features}
When finding a stop, blind travellers will make use of large physical objects to assist in navigation. As was discussed in \cite{Campbell2014}, crowdsourcing can be used to identify these features from Google Street View images or by visiting the locations. This information can then be provided to the blind user to help them determine when they have reached a particular stop.

\subsection*{Identifying orientation}
Bus stops often come in pairs (one for each direction of travel), which can be hard to distinguish from a stop name. By looking at the relative position of the road, we can identify which side of the traffic the user should be on.

\subsection*{Search without GPS}
Blind users may depend on their mobile device not running out of charge, so may be reluctant to activate GPS location services on their device. Utilising autocomplete as a text search option would allow them to find their location without GPS, or search for other locations to assist in preplanning objectives.

\subsection*{Stop by Stop directions}
Being able to look up particular bus routes and stops is not useful if you are not aware of what stops and routes you need to be using to get to your destination. Providing a list of the bus services that need to be taken to get to a destination is crucial for both pre-planned and ad-hoc route planning.

\subsection*{Favourites}
As aforementioned, searching using text or GPS is not ideal for various reasons. Commonly used stops and trips should be able to be stored for later recall on the go, especially useful if these can be stored through a desktop user interface.
